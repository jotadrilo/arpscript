class Reachable
	attr_reader :mac_m, :mac_r, :mac_h, :ip_r, :ip_h, :ip_m, :iface, :out
	def initialize(args={})
		@mac_m = args[:mac_m]
		@ip_m= args[:ip_m]
		@mac_r = args[:mac_r]
		@ip_r = args[:ip_r]
		@mac_h = args[:mac_h]
		@ip_h= args[:ip_h]

		@iface = args[:iface]
		@out = args[:out]
	end
end