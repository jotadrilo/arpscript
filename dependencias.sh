wget http://www.tcpdump.org/release/libpcap-1.4.0.tar.gz
tar -xf libpcap-1.4.0.tar.gz 
rm libpcap-1.4.0.tar.gz
cd libpcap-1.4.0
./configure --prefix=/usr && make
make install
aptitude install ruby1.9.1-dev
gem install pcaprub
gem install packetfu